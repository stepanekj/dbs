<?php

class AdminsController extends BaseController
{
    protected $layout = 'layout';

    public function listAdmins()
    {
        $admins = DB::select('select * from admins inner join users on (users.id = admins.user_id)');
        $this->layout->content = View::make('admins')->with('admins', $admins);
    }
}