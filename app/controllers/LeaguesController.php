<?php

class LeaguesController extends BaseController
{
    protected $layout = 'layout';

    public function listLeagues()
    {
        $leagues = DB::select('select * from leagues');
        $this->layout->content = View::make('leagues')->with(
            array(
                'leagues' => $leagues
            )
        );
    }

    public function listPossiblePlaces($leagueId)
    {
        $places = DB::select(
            'select places.id, places.name, places.adress, leagues.name as leaguename from places ' .
            'right join possible on (places.id = possible.place_id) ' .
            'inner join leagues on (leagues.id = possible.league_id) ' .
            'where leagues.id =' . $leagueId
        );

        $this->layout->content = View::make('possiblePlaces')
            ->with(array(
                'places' => $places,
                'league' => $places[0]->leaguename,
                'hasPlaces' => isset($places[0]->id),
            )
        );
    }

    public function listEnrolledPlayers($leagueId)
    {
        $players = DB::select('select players.user_id, players.name, users.username, leagues.name as leaguename from players ' .
            'inner join users on (users.id = players.user_id)' .
            'right join leagues on (players.enrolls_in_league = leagues.id) ' .
            'where leagues.id ='. $leagueId);

        $this->layout->content = View::make('enrolledPlayers')
            ->with(array(
                'players' => $players,
                'league' => $players[0]->leaguename,
                'hasPlayers' => isset($players[0]->user_id)
            ));
    }
} 