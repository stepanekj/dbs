<?php


class PlacesController extends BaseController
{
    protected $layout = 'layout';

    public function listPlaces()
    {
        $places = DB::select('select * from places');
        $this->layout->content = View::make('places')->with('places', $places);
    }
} 