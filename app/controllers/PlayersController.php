<?php

class PlayersController extends BaseController
{
    protected $layout = 'layout';

    public function listPlayers()
    {
        $players = DB::select('select * from players inner join users on (users.id = players.user_id)');
        $this->layout->content = View::make('players')->with('players', $players);
    }
}