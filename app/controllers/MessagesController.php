<?php


class MessagesController extends BaseController
{
    protected $layout = 'layout';

    public function listMessages()
    {
        $messages = DB::select('select messages.id, messages.text, sender.username as sender, recipient.username as recipient from messages ' .
            'inner join users sender on (messages.sender = sender.id) ' .
            'inner join users recipient on (messages.recipient = recipient.id)');

        $this->layout->content = View::make('messages')->with('messages', $messages);
    }
} 