<?php


class MatchesController extends BaseController
{
    protected $layout = 'layout';

    public function listMatches()
    {
        $sql = 'select matches.id, proposes.date, places.name as place from matches ' .
            'inner join proposes on (matches.id = proposes.match_id and proposes.agreed = true) ' .
            'inner join places on (places.id = proposes.place_id)' .
            'inner join users on (users.id = proposes.player_id)' .
            'order by proposes.date';

        $matches = DB::select($sql);

        $this->layout->content = View::make('matches')->with('matches', $matches);
    }

    public function showMatchDetail($matchId)
    {
        $sql = 'select * from players ' .
            'inner join played on (players.user_id = played.player_id)' .
            'inner join matches on (played.match_id = matches.id)' .
            'where matches.id = ' . $matchId;

        $result = DB::select($sql);

        $this->layout->content = View::make('matchesDetail')->with(
            array(
                'players' => $result,
            )
        );
    }
} 