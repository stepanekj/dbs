<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', 'HomeController@showWelcome');
Route::get('players', 'PlayersController@listPlayers');
Route::get('admins', 'AdminsController@listAdmins');
Route::get('places', 'PlacesController@listPlaces');
Route::get('leagues', 'LeaguesController@listLeagues');
Route::get('matches', 'MatchesController@listMatches');
Route::get('matches/result/{matchId}', 'MatchesController@showMatchDetail');
Route::get('messages', 'MessagesController@listMessages');
Route::get('leagues/possiblePlaces/{leagueId}', 'LeaguesController@listPossiblePlaces');
Route::get('leagues/enrolledPlayers/{leagueId}', 'LeaguesController@listEnrolledPlayers');
