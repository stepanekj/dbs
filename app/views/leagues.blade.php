@extends('layout')

@section('content')

<table>
    <tr>
        <th>id</th>
        <th>name</th>
    </tr>
    @foreach ($leagues as $league)
    <tr>
        <td>
            {{ $league->id }}
        </td>
        <td>
            {{ $league->name }}
        </td>
        <td>
            <a href="{{ action('LeaguesController@listPossiblePlaces', array($league->id)) }}">Mozna mista k poradani</a>
        </td>
        <td>
            <a href="{{ action('LeaguesController@listEnrolledPlayers', array($league->id)) }}">Zapsani hraci</a>
        </td>
    </tr>
    @endforeach
</table>
@stop