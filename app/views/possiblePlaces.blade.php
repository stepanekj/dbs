@extends('layout')

@section('content')
<h2>Liga {{ $league }} se muze hrat na techto mistech:</h2>
@if ($hasPlaces)
<table>
    <tr>
        <th>id</th>
        <th>jmeno</th>
        <th>adresa</th>

    </tr>
@foreach ($places as $place)
    <tr>
        <td>
            {{ $place->id }}
        </td>
        <td>
            {{ $place->name }}
        </td>
        <td>
            {{ $place->adress }}
        </td>
    </tr>
@endforeach
</table>
@else
Liga nema zadna mista, kde by se mohla konat!
@endif
@stop