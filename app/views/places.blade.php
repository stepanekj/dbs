@extends('layout')

@section('content')

<table>
    <tr>
        <th>id</th>
        <th>name</th>
    </tr>
    @foreach ($places as $place)
    <tr>
        <td>
            {{ $place->id }}
        </td>
        <td>
            {{ $place->name }}
        </td>
    </tr>
    @endforeach
</table>
@stop