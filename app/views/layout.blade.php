<html>
<body>
<h1>DBS semestralni prace</h1>

@section('nav_menu')
<ul>
    <li>
        <a href="{{ action('HomeController@showWelcome') }}">Home</a>
    </li>
    <li>
        <a href="{{ action('PlayersController@listPlayers') }}">Hraci</a>
    </li>
    <li>
        <a href="{{ action('AdminsController@listAdmins') }}">Administratori</a>
    </li>
    <li>
        <a href="{{ action('PlacesController@listPlaces') }}">Mista pro zapasy</a>
    </li>
    <li>
        <a href="{{ action('LeaguesController@listLeagues') }}">Ligy</a>
    </li>
    <li>
        <a href="{{ action('MatchesController@listMatches') }}">Zapasy</a>
    </li>
    <li>
        <a href="{{ action('MessagesController@listMessages') }}">Zpravy</a>
    </li>
</ul>
@show

@yield('content', 'Vitejte na strankach')
</body>
</html>