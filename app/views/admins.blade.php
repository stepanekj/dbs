@extends('layout')

@section('content')

<table>
    <tr>
        <th>id</th>
        <th>username</th>
        <th>email</th>
    </tr>
@foreach ($admins as $admin)
    <tr>
        <td>
            {{ $admin->id }}
        </td>
        <td>
            {{ $admin->username }}
        </td>
        <td>
            {{ $admin->email }}
        </td>
    </tr>
@endforeach
</table>
@stop