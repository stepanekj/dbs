@extends('layout')

@section('content')

<table>
    <tr>
        <th>id</th>
        <th>odesilatel</th>
        <th>prijemce</th>
        <th>text zpravy</th>
    </tr>
    @foreach ($messages as $message)
    <tr>
        <td>
            {{ $message->id }}
        </td>
        <td>
            {{ $message->sender }}
        </td>
        <td>
            {{ $message->recipient }}
        </td>
        <td>
            {{ $message->text }}
        </td>
    </tr>
    @endforeach
</table>
@stop