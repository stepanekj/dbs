@extends('layout')

@section('content')
<h2>Hraci zapsani do ligy {{ $league }} :</h2>

@if ($hasPlayers)
<table>
    <tr>
        <th>id</th>
        <th>uzivatelske jmeno</th>
        <th>jmeno</th>

    </tr>
    @foreach ($players as $player)
    <tr>
        <td>
            {{ $player->user_id }}
        </td>
        <td>
            {{ $player->username }}
        </td>
        <td>
            {{ $player->name }}
        </td>
    </tr>
    @endforeach
</table>
@else
Liga nema zadne zapsane hrace!
@endif

@stop