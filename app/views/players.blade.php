@extends('layout')

@section('content')

<table>
    <tr>
        <th>id</th>
        <th>username</th>
        <th>name</th>
        <th>email</th>
    </tr>
@foreach ($players as $player)
    <tr>
        <td>
            {{ $player->id }}
        </td>
        <td>
            {{ $player->username }}
        </td>
        <td>
            {{ $player->name }}
        </td>
        <td>
            {{ $player->email }}
        </td>
    </tr>
@endforeach
</table>
@stop