@extends('layout')

@section('content')

<table>
    <tr>
        <th>id</th>
        <th>datum konani</th>
        <th>misto konani</th>
    </tr>
    @foreach ($matches as $match)
    <tr>
        <td>
            {{ $match->id }}
        </td>
        <td>
            {{ $match->date }}
        </td>
        <td>
            {{ $match->place }}
        </td>
        <td>
            <a href="{{ action('MatchesController@showMatchDetail', array($match->id)) }}">Vysledky</a>
        </td>
    </tr>
    @endforeach
</table>
@stop