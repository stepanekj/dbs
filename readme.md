### Semestralni prace predmetu dbs

Pro nainstalovani projektu je treba nastavit web serveru root directory jako ./server.php.

V souboru ./dbs.nginx je ukazka nginx hostu. Staci vyplnit cestu k adresari s projektem

Stahnout composer 'curl -sS https://getcomposer.org/installer | php'

V korenovem adresari spustit 'php composer.phar install'

Pripojeni k databazi predpoklada postgre na localhostu. Uzivatel 'postgres', heslo '1234' .

Pro spravny beh doporucuji php 5.4 , dale je nezbytne nutna php extensiona 'mcrypt'.

A konecne je treba nastavit prava pro adresar projektu, aby se do nej dostal www-data a mohl zapisovat do app/storage.